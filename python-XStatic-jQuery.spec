%global _empty_manifest_terminate_build 0
Name:		python-XStatic-jQuery
Version:	3.5.1.1
Release:	1
Summary:	jQuery 1.12.4.1 (XStatic packaging standard)
License:	MIT
URL:		https://jquery.com/
Source0:	https://files.pythonhosted.org/packages/f0/f9/5ec13f2545f84f5616dc1e5ff65049e4094db6a18f7f33c2c7fb5fb57c16/XStatic-jQuery-3.5.1.1.tar.gz
BuildArch:	noarch


%description
jQuery javascript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-jQuery
Summary:	jQuery 1.12.4.1 (XStatic packaging standard)
Provides:	python-XStatic-jQuery = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-XStatic-jQuery
jQuery javascript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:	Development documents and examples for XStatic-jQuery
Provides:	python3-XStatic-jQuery-doc
%description help
jQuery javascript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-jQuery-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-jQuery -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Dec 06 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 3.5.1.1-1
- Update package to version 3.5.1.1

* Sat Jan 30 2021 liusheng <liusheng2048@gmail.com> - 1.12.4.1-1
- Initial package
